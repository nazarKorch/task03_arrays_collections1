package collections;

import collections.Droid;

public class Humanoid extends Droid {
  private String name;

  public Humanoid(String name){
    super(name);
    this.name = name;
  }

}
