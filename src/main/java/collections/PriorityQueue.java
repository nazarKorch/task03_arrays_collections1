package collections;

import java.util.Comparator;
import java.util.*;
import java.util.Queue;

public class PriorityQueue<T extends Droid> {

  private Queue<T> queue;

  public PriorityQueue() {
    queue = new java.util.PriorityQueue<>(comparator);
  }

  public boolean addDroid(T droid){
     return queue.add(droid);
  }

  public boolean offerDroid(T droid){
    return queue.offer(droid);
  }

  public boolean removeDroid(T droid){
    return queue.remove(droid);
  }

  public T peekDroid(){
    return queue.peek();
  }

  public T pollDroid(){
   return queue.poll();
  }

  public boolean isEmpty(){
    return queue.isEmpty();
  }

  public int size(){
    return queue.size();
  }


  Comparator<T> comparator = (T a, T b) -> {

    return (a.getClass().toString().compareTo(b.getClass().toString()));

  };
}
