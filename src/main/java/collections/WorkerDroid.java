package collections;

import collections.Droid;

public class WorkerDroid extends Droid {

  private String name;

  public WorkerDroid(String name) {
    super(name);
    this.name = name;
  }

}
