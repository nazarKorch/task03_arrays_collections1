package Arrays;

import java.util.Random;


public class Main {

  public static RoundRoom randomizeArtifacts() {
    RoundRoom roundRoom = new RoundRoom();
    BehindTheDoor behindTheDoor;

    Random random = new Random();

    for (int i = 0; i < 10; i++) {

      switch (random.nextInt(5)) {
        case 1: {
          behindTheDoor = new Monster(random.nextInt(95) + 5);
          roundRoom.setElement(behindTheDoor);
          break;
        }
        default: {
          behindTheDoor = new Artefact(random.nextInt(70) + 10);
          roundRoom.setElement(behindTheDoor);
          break;
        }
      }


    }
    return roundRoom;
  }

  public static void showWhatIsBehind(RoundRoom roundRoom) {
    for (int i = 0; i < 10; i++) {
      System.out.println("Behind " + i + " door is: " + roundRoom.getElement(i).toString()
          + " with power - " + roundRoom.getElement(i).getPower());
    }
  }

  public static void findDangerousDoors(RoundRoom roundRoom) {
    int sum = 0;
    Character character;
    for (int i = 0; i < 10; i++) {

      character = new Character(roundRoom.getElement(i));
      if (Character.power == 0) {
        sum++;
      }
    }
    System.out.println("Find " + sum + " dangerous door(s)");
  }

  public static void showDoorsToAlive(RoundRoom roundRoom) {
    int tmp = 0;
    int t = -1;
    for (int i = 0; i < 10; i++) {

      if (roundRoom.getElement(i) instanceof Monster) {
        if (roundRoom.getElement(i).getPower() > Character.power) {
          t = i;
          tmp++;
        } else {
          System.out.print((i + 1) + " door - ");
          new Character(roundRoom.getElement(i));
        }
      } else {
        System.out.print((i + 1) + " door - ");
        new Character(roundRoom.getElement(i));
      }

      if (t != -1) {

        if (roundRoom.getElement(t).getPower() > Character.power) {
          System.out.println("for " + (t + 1) + " door you have not enough power");
          if (i == 9) {
            System.out.println("You have been killed by monster!");
          }
        } else {
          System.out.print((t + 1) + " door - ");
          new Character(roundRoom.getElement(t));
          if (tmp == 1) {
            t = -1;
          } else {
            t -= 1;
            tmp -= 1;
          }
        }
      }


    }
  }


  public static void main(String[] args) {

    RoundRoom doors = randomizeArtifacts();
//    showWhatIsBehind(doors);
//    findDangerousDoors(doors);
    showDoorsToAlive(doors);
  }

}
