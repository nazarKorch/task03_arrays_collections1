package Arrays;

public class ArrayMaker {

  private String[] sum;

  public ArrayMaker() {

  }

  public <T> String[] addTwoArrays(T[] array1, T[] array2) {

    int length = array1.length + array2.length;
    sum = new String[length - 1];

    for (int i = 0; i < length; i++) {
      if (i < array1.length) {
        sum[i] = array1[i].toString();
      } else {
        sum[i] = array2[i].toString();
      }
    }
    return sum;
  }

  public <T> T[] deleteEqualNumbers(T[] array) {
    int k = 0;
    int count = 0;
    for (int i = 0; i < array.length; i++) {
      for (int j = array.length - 1; j > i; j--) {
        if (array[i] == array[j]) {
          count++;
          if (count == 2) {
            array[i] = null;
            k++;
          }
        }
      }
      count = 0;
    }
    return array;
  }

  public <T> T[] deleteEveryEqual(T[] array) {

    for (int i = 0; i < array.length; i++) {
      for (int j = array.length - 1; j > i; j--) {
        if (array[i] == array[j]) {
          array[i] = null;
        }
      }
    }

    return array;
  }
}