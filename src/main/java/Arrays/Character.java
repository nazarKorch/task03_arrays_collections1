package Arrays;

public class Character {

  static int power = 25;

  public <T extends BehindTheDoor> Character(T element) {

    if (element instanceof Artefact) {
      Character.power += element.getPower();
      System.out.println("You gained " + element.getPower() + ", your power now: "
          + power);
    } else {
      if (element.getPower() > Character.power) {
        System.out.println("You are dead!");
        power = 0;
      } else {
        Character.power -= element.getPower();
        System.out.println("You win the fight! Your power now: " + power);
      }
    }
  }

}
