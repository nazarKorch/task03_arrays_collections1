package Arrays;

public class Monster extends BehindTheDoor {

  private int power;

  public Monster(int power) {
    if ((power < 5) || (power > 100)) {
      System.out.println("The power is not right!");
    } else {
      this.power = power;
    }
  }

  public int getPower() {
    return power;
  }

}
