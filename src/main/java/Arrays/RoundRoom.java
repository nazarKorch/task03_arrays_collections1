package Arrays;

public class RoundRoom {

  private BehindTheDoor[] array = new BehindTheDoor[10];
  static int count = 0;

  public RoundRoom() {

  }

  public void setElement(BehindTheDoor element) {
    if (count < 10) {
      array[count] = element;
      count++;
    } else {
      System.out.println("OutOfBounds!");
    }
  }

  public BehindTheDoor getElement(int index) {
    return array[index];
  }


}
