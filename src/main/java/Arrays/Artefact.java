package Arrays;

public class Artefact extends BehindTheDoor {

  private int power;

  public Artefact(int power) {
    this.power = power;
  }

  public int getPower() {
    return power;
  }


}
