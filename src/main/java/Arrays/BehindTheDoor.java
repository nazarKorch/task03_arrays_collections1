package Arrays;

public abstract class BehindTheDoor {

  public abstract int getPower();

  public String toString() {
    return this.getClass().getSimpleName();
  }
}
